package com.example.randomgo

import org.osmdroid.util.GeoPoint
import kotlin.random.Random

class RandomPathGenerator(
    var deviation: Double,
) {
    fun generateRandomPath(
        source: GeoPoint,
        destination: GeoPoint,
        numberOfPoints: Int
    ): ArrayList<GeoPoint> {
        val points = ArrayList<GeoPoint>()
        if (deviation > 0.0) {
            for (i in 0 until numberOfPoints) {
                points.add(randomPointBetweenPoints(source, destination, deviation))
            }
        }
        return points
    }

    fun randomPointBetweenPoints(
        pointA: GeoPoint,
        pointB: GeoPoint,
        deviationFactor: Double
    ): GeoPoint {
        val latA = pointA.latitude
        val longA = pointA.longitude
        val latB = pointB.latitude
        val longB = pointB.longitude

        val latDiff = latB - latA
        val longDiff = longB - longA

        val middlePoint = GeoPoint(latA + latDiff / 2, longA + longDiff / 2)

        val radius = Math.sqrt(latDiff * latDiff + longDiff * longDiff) / 2
        val maxDeviationDistance = deviationFactor * radius

        val latDeviation = Random.nextDouble(
            -maxDeviationDistance,
            maxDeviationDistance
        )
        val longDeviation = Random.nextDouble(
            -maxDeviationDistance,
            maxDeviationDistance
        )

        return GeoPoint(
            middlePoint.latitude + latDeviation,
            middlePoint.longitude + longDeviation
        )
    }
}