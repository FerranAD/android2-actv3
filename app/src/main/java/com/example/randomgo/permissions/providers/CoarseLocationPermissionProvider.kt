package com.example.randomgo.permissions.providers

import android.Manifest
import com.example.randomgo.permissions.providers.text.CoarseLocationTextProvider

class CoarseLocationPermissionProvider: PermissionProvider {
    override val textProvider = CoarseLocationTextProvider()
    override val permission = Manifest.permission.ACCESS_COARSE_LOCATION
}