package com.example.randomgo.permissions.providers

import android.Manifest
import com.example.randomgo.permissions.providers.text.FineLocationTextProvider

class FineLocationPermissionProvider: PermissionProvider {
    override val textProvider = FineLocationTextProvider()
    override val permission = Manifest.permission.ACCESS_FINE_LOCATION
}