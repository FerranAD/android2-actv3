package com.example.randomgo.permissions.providers

import com.example.randomgo.permissions.providers.text.PermissionTextProvider

interface PermissionProvider {
    val textProvider: PermissionTextProvider
    val permission: String
}