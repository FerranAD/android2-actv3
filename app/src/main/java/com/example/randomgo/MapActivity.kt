package com.example.randomgo

import MapView
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.lifecycleScope
import com.example.randomgo.location.DefaultLocationClient
import com.example.randomgo.location.LocationClient
import com.example.randomgo.ui.theme.RandomGoTheme
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.osmdroid.bonuspack.routing.OSRMRoadManager
import org.osmdroid.bonuspack.routing.RoadManager
import org.osmdroid.events.MapEventsReceiver
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.MapEventsOverlay
import org.osmdroid.views.overlay.Marker


const val MY_USER_AGENT = "RandomGo"


val DEFAULT_GEOPOINT = GeoPoint(0.0, 0.0)
val DEFAULT_DEVIATION = 0.0
var IS_CLEAR_CLIKED = mutableStateOf(false)

class MapActivity : ComponentActivity() {
    private lateinit var locationClient: LocationClient
    private var currentLocation = mutableStateOf(GeoPoint(0.0, 0.0))
    var roadManager = OSRMRoadManager(this, MY_USER_AGENT)
    val pathGenerator = RandomPathGenerator(DEFAULT_DEVIATION)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val policy = ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        roadManager.setMean(
            OSRMRoadManager.MEAN_BY_FOOT
        )
        getCurrentLocation()
        setContent {
            RandomGoTheme {
                val currentLocation by remember { currentLocation }
                if (currentLocation != DEFAULT_GEOPOINT)
                    CurrentLocationMapView(
                        source = currentLocation,
                        generator = pathGenerator,
                        roadManager = roadManager,
                    )
            }
        }
    }


    private fun getCurrentLocation() {
        locationClient = DefaultLocationClient(
            applicationContext,
            LocationServices.getFusedLocationProviderClient(applicationContext)
        )
        var updates = 0
        locationClient
            .getLocationUpdates(10000L)
            .catch { e -> e.printStackTrace() }
            .onEach { location ->
                if (updates < 1) updates++ else return@onEach
                currentLocation.value = GeoPoint(location.latitude, location.longitude)
            }.launchIn(this.lifecycleScope)
    }
}

@Composable
fun CurrentLocationMapView(
    modifier: Modifier = Modifier,
    source: GeoPoint = DEFAULT_GEOPOINT,
    generator: RandomPathGenerator,
    roadManager: RoadManager,
) {
    var destination = DEFAULT_GEOPOINT
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        MyMapView(
            modifier = Modifier.weight(8f),
            source = source,
            generator = generator,
            roadManager = roadManager
        )
        Button(
            onClick = { IS_CLEAR_CLIKED.value = true },
            modifier = Modifier
                .weight(1f)
                .padding(top = 4.dp)
                .wrapContentSize()
        ) {
            Text(
                text = "Clear",
                style = MaterialTheme.typography.bodyLarge,
            )
        }
        MySlider(
            generator = generator,
            modifier = Modifier
                .padding(start = 16.dp, end = 16.dp, bottom = 10.dp)
                .weight(1f)
        )
    }
}

@Composable
fun MySlider(
    modifier: Modifier = Modifier,
    generator: RandomPathGenerator,
) {
    var currentValue by remember { mutableStateOf(0.0f) }
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.Start,
    ) {
        Text(
            text = "Slide for randomness",
            modifier = Modifier.padding(start = 34.dp),
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Medium,
            fontFamily = FontFamily.SansSerif
        )
        Slider(
            modifier = modifier.padding(start = 16.dp),
            valueRange = 0.0f..2.0f,
            value = currentValue,
            onValueChange = { value ->
                currentValue = value
                generator.deviation = value.toDouble()
            }
        )
    }
}

@Composable
fun MyMapView(
    modifier: Modifier = Modifier,
    source: GeoPoint = DEFAULT_GEOPOINT,
    generator: RandomPathGenerator,
    roadManager: RoadManager,
) {
    val isClearClicked by remember { IS_CLEAR_CLIKED }
    MapView(
        modifier = modifier
            .padding(16.dp)
            .shadow(
                elevation = 16.dp,
                shape = MaterialTheme.shapes.medium,
            ),
    ) {
        if (isClearClicked) {
            it.overlays.clear()
            IS_CLEAR_CLIKED.value = false
        }
        it.controller.apply {
            setZoom(15.0)
            animateTo(source, 18.0, 1000L)
        }
        it.addMarker(source)
        it.overlays.add(MapEventsOverlay(object : MapEventsReceiver {
            override fun longPressHelper(p: GeoPoint?): Boolean {
                return true
            }

            override fun singleTapConfirmedHelper(p: GeoPoint?): Boolean {
                if (p == null) return true
                it.addMarker(p)
                it.addRandomRoad(generator, roadManager, source, p)
                return true
            }
        }))
    }
}

fun MapView.addMarker(geoPoint: GeoPoint) {
    val marker = Marker(this)
    marker.position = geoPoint
    marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
    this.overlays.add(marker)
}

fun MapView.addRandomRoad(
    generator: RandomPathGenerator,
    roadManager: RoadManager,
    source: GeoPoint,
    destination: GeoPoint
) {
    val waypoints = ArrayList<GeoPoint>().apply {
        add(source);
        generator.generateRandomPath(
            source,
            destination,
            1
        ).forEach { point ->
            add(point)
        }
        add(destination)
    }
    val road = roadManager.getRoad(waypoints)
    this.overlays.add(RoadManager.buildRoadOverlay(road))
}
