package com.example.randomgo

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.randomgo.permissions.PermissionRequester
import com.example.randomgo.permissions.RequestPermissionContext
import com.example.randomgo.permissions.providers.CoarseLocationPermissionProvider
import com.example.randomgo.permissions.providers.FineLocationPermissionProvider
import com.example.randomgo.ui.theme.RandomGoTheme

class MainActivity : ComponentActivity() {
    private val permissionsToRequest = listOf(
        FineLocationPermissionProvider(),
        CoarseLocationPermissionProvider()
    )
    private lateinit var permissionRequester: PermissionRequester

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        permissionRequester = PermissionRequester(this, permissionsToRequest)
        setContent {
            RandomGoTheme {
                RequestPermissionContext(permissionRequester) {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        HomeMenu {
                            requestPermissions()
                            if (hasLocationPermision()) startActivity<MapActivity>()
                        }
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeMenu(
    start: () -> Unit,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        Text(
            text = "RandomGo, the app that helps you find a new route to your destination.",
            style = MaterialTheme.typography.headlineLarge,
            color = MaterialTheme.colorScheme.onPrimaryContainer,
            fontStyle = FontStyle.Italic,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(top = 50.dp, start = 30.dp, end = 30.dp, bottom = 170.dp)
        )
        Button(onClick = start) {
            Text(
                text = "Start",
                style = MaterialTheme.typography.headlineSmall,
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    RandomGoTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.primaryContainer
        ) {
            HomeMenu {}
        }
    }
}